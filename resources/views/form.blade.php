﻿<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>WhatsApp Marketing - Start Comunicação</title>
    </head>

    <body class="blurBg-false" style="background-color:#EBEBEB">
        <!-- Start Formoid form-->
        <link rel="stylesheet" href="https://formoid.net/forms/02/281097/formoid-solid-light-green.css" type="text/css"/>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <script type="text/javascript" src="https://formoid.net/lib/iframe.js"></script>
        <link rel="stylesheet" href="https://formoid.net/lib/form.css" type="text/css"/>
        <form enctype="multipart/form-data" class="formoid-solid-light-green"
              style="background-color:#FFFFFF;font-size:11px;font-family:'Roboto',Arial,Helvetica,sans-serif;color:#34495E;max-width:480px;min-width:150px"
              method="post">
            <div class="title"><h2>WhatsApp Marketing</h2></div>
            <div class="element-number" title="(DDD) + Número"><label class="title"><span class="required">*</span></label>
                <div class="item-cont"><input class="large" type="text" min="1" max="100" name="number" required="required"
                                              placeholder="Números para enviar (Máximo 15)" value=""/><span
                        class="icon-place"></span></div>
            </div>
            <div class="element-textarea"><label class="title"><span class="required">*</span></label>
                <div class="item-cont"><textarea class="medium" name="textarea" cols="20" rows="5" required="required"
                                                 placeholder="Mensagem a ser enviada"></textarea><span
                        class="icon-place"></span></div>
            </div>
            <div class="element-file"><label class="title"></label>
                <div class="item-cont"><label class="large">
                    <div class="button">Selecione uma mídia</div>
                    <input type="file" class="file_input" name="file"/>
                    <div class="file_text">Imagem, Video ou PDF</div>
                    <span class="icon-place"></span></label></div>
            </div>
            <div class="submit"><input type="submit" value="Enviar"/></div>
            <div id="formoid-form-footer">
                <ul id="formoid-links">
                </ul>
            </div>
        </form>

    </body>
</html>
