<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Mail\SendGregMail as GregMail;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function mail()
    {
        return view('form');
    }

    public function sendmail(Request $request)
    {
        $nums = $request->input('number');
        $txt  = $request->input('textarea');
        $data = [
            'document' => $request->file
        ];

        Mail::send(
            new GregMail(
                'rodsoadev@gmail.com',
                $nums,
                $txt,
                $data
            )
        );
    }
}
