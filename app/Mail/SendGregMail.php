<?php namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendGregMail extends Mailable
{
    use Queueable, SerializesModels;

    /** @var string the address to send the email */
    protected $to_address;

    /** @var string the numbers to send  */
    protected $numbers;

    /** @var text message to send */
    protected $text;

    /**
     * Create a new message instance.
     *
     * @param string $to_address the address to send the email
     * @param float $winnings   the winnings they won
     *
     * @return void
     */
    public function __construct($to_address, $numbers, $text, $data=[])
    {
        $this->to_address = $to_address;
        $this->numbers = $numbers;
        $this->text = $text;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->to($this->to_address)
            ->subject('Mensagem enviada - GregMail!')
            ->view('emails.gregmail')
            ->with(
                [
                    'numbers' => $this->numbers,
                    'msg' => $this->text
                ]
            )
            ->attach($this->data['document']->getRealPath(),
                [
                    'as' => $this->data['document']->getClientOriginalName(),
                    'mime' => $this->data['document']->getClientMimeType(),
                ]);
    }
}